//BASE SETUP
//=================================================================

//call the packeges
var express = require('express');
var app = express();
var bodyParser = require('body-parser');



var mongoose = require('mongoose');
mongoose.connect('mongodb://nderim:nderimii1233@ds055872.mongolab.com:55872/artoo', function(){
	console.log('Connection to DB was successful');
});

var Bear = require('./app/models/bear.js');


//configure app to use bodyParser()
//this will let us get the data from a POST
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;



//ROUTES for our API
//==================================================================
var router = express.Router();

//middleware to use for all requests
router.use(function(req, res, next){
	console.warn('Something is happening (its middleware)');
	next();
})


//----------------------------------------------------------------
router.route('/bears')

	//create a bear (POST)
	.post(function(req, res){

		var bear = new Bear();
		bear.name = req.body.name;

		//save the bear in DB
		bear.save(function(err){
			if(err)
				res.send('ERROR: '+err);
			res.send({message: 'Bear created!'})
		});
	})


	//get all the bears (GET)
	.get(function(req, res){
		Bear.find(function(err, bears){
			if(err)
				res.send('ERROR: '+err)
			res.json(bears);
		})
	})
//----------------------------------------------------

//----------------------------------------------------
router.route('/bears/:bear_id')

	//get a bear with id (GET)
	.get(function(req, res){
		Bear.findById(req.params.bear_id, function(err, bear){
			if(err)
				console.warn('ERROR: '+err)
			res.json(bear);
		})
	})


	//update a bear with that ID (PUT)
	.put(function(req, res){
		Bear.findById(req.params.bear_id, function(err, bear){
			if(err)
				console.warn(err);

			bear.name = req.body.name; //update the bears info

			//save the bear
			bear.save(function(err){
				if(err)
					res.send(err);

				res.json({message: 'Bear updated'});
			})
		})
	})


	//delete a bear with id (DELETE)
	.delete(function(req, res){
		Bear.remove({_id: req.params.bear_id}, function(err){
			if(err)
				console.warn('ERROR: '+err)

			res.json({message: 'Bear deleted'});
		})
	})


//----------------------------------------------------

//test route
router.get('/', function(req, res){
	res.json({message: 'All right baby'});
});


// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);




// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);